from setuptools import setup, find_packages

from Findclone import __version__ as version

setup(
    name="findclone_api",
    version="0.2.2",
    python_requires=">=3.6",
    packages=find_packages(),
    install_requires=['requests==2.26.0',
                      'aiohttp==3.7.4.post0',
                      'Pillow==8.3.2'],
)
